import React, { Component } from 'react';
import { StyleSheet, View, Button, ScrollView } from 'react-native';

export default class HomeScreen extends Component {
  render() {
    return (
      <ScrollView>
      <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
          <Button title="ActivityIndicator" color="#042bb7" onPress={() => this.props.navigation.navigate('ActivityIndicator')} /></View>
       <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
          <Button title="Button" color="#042bb7" onPress={() => this.props.navigation.navigate('Button')} /></View>
      <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
          <Button title="Details" color="#042bb7" onPress={() => this.props.navigation.navigate('Details')} /></View>
       <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
          <Button title="DrawerLayoutAndroid" color="#042bb7" onPress={() => this.props.navigation.navigate('DLA')} /></View>
        <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
        <Button title="Image" color="#042bb7" onPress={() => this.props.navigation.navigate('Image')} /></View>
        <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
      <Button title="KeyboardAvoidingView" color="#042bb7" onPress={() => this.props.navigation.navigate('KAV')} /></View >
        <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
      <Button title="ListView" color="#042bb7" onPress={() => this.props.navigation.navigate('ListView')} /></View >
        <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
      <Button title="Modal" color="#042bb7" onPress={() => this.props.navigation.navigate('Modal')} /></View >
        <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
      <Button title="Picker" color="#042bb7" onPress={() => this.props.navigation.navigate('Picker')} /></View >
        <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
      <Button title="ProgressBarAndroid" color="#042bb7" onPress={() => this.props.navigation.navigate('PBA')} /></View >
        <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
      <Button title="RefreshControl" color="#042bb7" onPress={() => this.props.navigation.navigate('RefreshControl')} /></View >
        <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
      <Button title="ScrollView" color="#042bb7" onPress={() => this.props.navigation.navigate('ScrollView')} /></View >
        <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
      <Button title="SectionList" color="#042bb7" onPress={() => this.props.navigation.navigate('SectionList')} /></View >
        <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
      <Button title="Slider" color="#042bb7" onPress={() => this.props.navigation.navigate('Slider')} /></View >
        <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
      <Button title="StatusBar" color="#042bb7" onPress={() => this.props.navigation.navigate('StatusBar')} /></View >
        <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
      <Button title="Switch" color="#042bb7" onPress={() => this.props.navigation.navigate('Switch')} /></View >
        <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
      <Button title="Text" color="#042bb7" onPress={() => this.props.navigation.navigate('Text')} /></View >
        <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
      <Button title="TextInput" color="#042bb7" onPress={() => this.props.navigation.navigate('TextInput')} /></View >
        <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
      <Button title="TouchableHighlight" color="#042bb7" onPress={() => this.props.navigation.navigate('TH')} /></View >
        <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
      <Button title="TouchableNativeFeedback" color="#042bb7" onPress={() => this.props.navigation.navigate('TNF')} /></View >
        <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
      <Button title="TouchableOpacity" color="#042bb7" onPress={() => this.props.navigation.navigate('TO')} /></View >
        <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
      <Button title="TouchableWithoutFeedback" color="#042bb7" onPress={() => this.props.navigation.navigate('TWF')} /></View >
        <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
      <Button title="View" color="#042bb7" onPress={() => this.props.navigation.navigate('View')} /></View >
        <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
      <Button title="ViewPagerAndroid" color="#042bb7" onPress={() => this.props.navigation.navigate('VPA')} /></View >
        <View style={[{ width: "80%", margin: 10, paddingLeft: 50 }]}>
      <Button title="WebView" color="#042bb7" onPress={() => this.props.navigation.navigate('WebView')} /></View >
     
      </ScrollView>
    );
  }
}
